# Vorlesungswoche 8 (31. Mai – 6. Juni) #

Handnotizen findet man unter [notes/woche8.pdf](./../notes/woche8.pdf).

## Agenda ##

- [x] 2–5 min Organisatorisches
    - Feedback zu HA 1
    - Symbolverzeichnis findet man in [notes/glossar.md](./../notes/glossar.md).
- [x] Serie 3, Seminaraufgaben besprechen
    - [x] SemA 3.1
    - [x] SemA 3.2
    - [x] SemA 3.3
- [ ] restliche Zeit für allg. Fragen

## Nächste Woche ##

- HA aus Blatt 2.

### TODOs (Studierende) ###

– VL Wochen 5+6 wieder durchlesen, VL Woche 7 für Vorlesung durchlesen.
- am ÜB 3 weiter arbeiten.
