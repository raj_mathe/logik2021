# Vorlesungswoche 10 (14.–20. Juni) #

Handnotizen findet man unter [notes/woche10.pdf](./../notes/woche10.pdf).

## Agenda ##

- [x] 2–5 min Organisatorisches
- [x] Serie 4, Seminaraufgaben besprechen
    - [x] SemA 4.1
    - [x] SemA 4.2
    - [x] SemA 4.3
    - [x] SemA 4.4
    - [x] SemA 4.5
    - Anmerkungen zu HA
- [x] restliche Zeit für allg. Fragen

## Nächste Woche ##

- HA aus Blatt 3.

### TODOs (Studierende) ###

- am ÜB 4 weiter arbeiten.
