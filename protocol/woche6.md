# Vorlesungswoche 6 (17.–23. Mai) #

## Agenda ##

- [x] 2–5 min Organisatorisches
- [x] Serie 1, Hausaufgaben besprechen (inkl. Feedbacknotizen von SHK)
    - [x] HA 1.1
    - [x] HA 1.2
    - [x] HA 1.3
    - [x] HA 1.4
- [x] restliche Zeit für allg. Fragen
    - Fragen zu strukturelle Induktion

## Nächste Woche ##

- nach Anfrage kann ggf. etwas aufgezeichnet werden

### TODOs (Studierende) ###

- ÜB 2 bis **Do 20.05.** um 22:00 abgeben!
