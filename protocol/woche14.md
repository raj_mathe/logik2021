# Vorlesungswoche 14 (12.–18. Juli) #

## Agenda ##

- [x] 2–5 min Organisatorisches
    - **Beachte:** HA zu Serie 6 werden leider nicht besprochen!
    - Notenstand in Moodle, Schwellwert 59 Pkt
    - Zusatzübung
- [x] Serie 6, Seminaraufgaben besprechen
    - [x] SemA 6.1
    - [x] SemA 6.2
    - [x] SemA 6.3
    - [x] SemA 6.4
    - ~~[ ] (Zusatz) SemA 6.5~~ ---> siehe Aufzeichnung in Moodle
- [x] restliche Zeit für allg. Fragen

## Nächste Woche ##

- HA aus Blatt 5.

### TODOs (Studierende) ###

- am ÜB 6 weiter arbeiten.
