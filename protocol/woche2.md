# Vorlesungswoche 2 (19.–25. April) #

## Agenda ##

- [x] Organisatorisches
    - [x] Übungs / Seminaraufgaben + Takt
        ```
                             ÜB1 release_______Abgabe  ÜB2 release
                      heute  /                      |/
                          ↓ ↓                       ↓
        [    VL1    ][    VL2    ][    VL3    ][    VL4    ] ···
        Besprechung:     (orga)    Seminaraufg      ???
                                   aus Blatt1

        ··· [    VL5    ][    VL6    ][    VL7    ][    VL8    ] ···
             Seminaraufg  Hausaufg     Seminaraufg  Hausaufg
             aus Blatt2   aus Blatt1   aus Blatt3   aus Blatt2
        ```
    - [x] Klausur? (TBA)
    - [x] Plattformen + Ressourcen
        - Alma
        - Moodle
        - Discord
            - bleibt freundlich und respektvoll : )
            - gerne könnt ihr dort Kanäle sowie Sprachkanäle erstellen;
            - über **Sprachkanäle** können sich größere Gruppen üben;
            - **Gruppenfindung** darüber möglich.
        - ILIAS
        - BBB
        - Folien
        - Literatur: _stark empfohlen, aber kein Muss._
        - *Repository
    - [x] **Modus** der Übungsgruppen -> ggf. Umfrage
        - Tafel vs. Bildschirmübertragen: laut Umfrage wird beides gewollt
        - optimale Lösung: Bildschirmübertragung + iPad zum Skizzen (leider Problem beim Spiegeln).
- [x] Inhalte
    - [x] Motivation
    - [ ] Mathematische Grundlage (ab Seite 59 in den Folien)

## Nächste Woche ##

Das 1. Übungsblatt erhalten Teilnehmer+innen am Do 22.04.
In VL Woche 3 Woche besprechen wir die Seminaraufgaben daraus.

### TODOs (Studierende) ###

- Folien 1 + 2 durchlesen;
- (ab Do) das 1. Übungsblatt anschauen;
- sinnvoll wäre es: die Seminaraufgabe vorm nächsten Treffen zu probieren;
- Gruppen für Abgabe diese Woche noch festlegen.

### Addendum ###

- Eine Zoom-Lizenz habe ich von der Uni beantragt.
Das könnte ein paar Wochen dauern.
Falls ich die bekommen, können wir gerne darauf wechseln.
- Inzwischen habe ich einen Trick zur Übertragung von Tablett entdeckt.
Das wäre eine Lösung auf die **Modus** Frage in der Liste oben.
