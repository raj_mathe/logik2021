# Vorlesungswoche 3 (26. April – 2. Mai) #

## Agenda ##

- [x] Organisatorisches (max. 5 min)
    - Erinnerung über Abgabe: Matrikelnr., 1 PDF.
- [x] Serie 1, Seminaraufgaben
    - [x] SA 1.1: 5 Min
        - Typen von Ausdrücken
    - [x] SA 1.2: 10 Min
        - **§3 Defn** aus VL2 (rekursiver Aufbau)
        - Teilformeln
        - Kurzhand-Konventionen (‘abuse of notation’) ... aber für _dieses Blatt_ noch nicht!!
        - Endlichkeit
        - Atome: inhaltsleer, aber Schnittstelle, um komplexe Zusammenhänge zu kodieren.
    - [x] SA 1.3: 10 Min
        - Interpretation
            - Bedeutung??
            - Konzept: Interpretation vs. Strukturen („Modelle“)
            - Interpretation als Menge od. Fkt (Belegung)
            - Interpretation vs. eval (erzeugt)
        - funktionale Semantik als primäre Definition (VL2, Folie 43)
            ... Wahrheitstabelle _nur_ als Rechenverfahren
        - Rechentricks in Wahrheitstabellen
            - ¬: kehre alle Werte um.
            - [Spalte 1] ∨ [Spalte 2]: alles 1, außer Zeilen wo beide Spalten 0
            - [Spalte 1] ∨ [Spalte 2]: alles 0, außer Zeilen wo beide Spalten 1
            - [Spalte 1] ⟶ [Spalte 2]: alles 1, außer Zeilen wo [Spalte 1]=1 und [Spalte 1]=2
            - [Spalte 1] ⟷ [Spalte 2]: 1, wo Spalte1 = Spalte 2, sonst 0
        - **Lösung:**
            ```
            | A₁ A₂ A₃ | ¬A₂  | A₃ ⟶ ¬A₂ | A₁ ∨ (A₃ ⟶ ¬A₂) | F   |
            | :------- | :--- | :--------- | :--------------- | :-- |
            | 0  0  0  |  1   |     1      |    1             |  0  |
            | 0  0  1  |  1   |     1      |    1             |  0  |
            | 0  1  0  |  0   |     1      |    1             |  0  |
            | 0  1  1  |  0   |    *0      |   *0             |  1  | <---
            | 1  0  0  |  1   |     1      |    1             |  0  |
            | 1  0  1  |  1   |     1      |    1             |  0  |
            | 1  1  0  |  0   |     1      |    1             |  0  |
            | 1  1  1  |  0   |    *0      |    1             |  0  |
            ```
            **Beachte:** In den Spalten mit \* musste man nur diese Zeilen mit Fällen
            (1 ⟶ 0) bzw. (0 ∨ 0) aussuchen und auf 0 setzen,
            für alle anderen Zeilen wird auf 1 gesetzt.

    - [-] SA 1.4: 10 Min
        - Entscheidungsverfahren anhand Wahrheitstabellen, ob (vgl. **§10 Defn**)
            - F tautologisch ?
            - F erfüllbar, und I |= F ?
                - Frage: Heißt erfüllbar, dass es **genau 1** Interp. gibt??
                - |= sem. Folgerungsbeziehung / sem. entailment (-> Modelle)
                - |- synt. Folgerungsbeziehung / synt. entailment (-> Kalkül)
            - F widerlegbar
            - F nicht erfüllbar
        - Am Rand: Algorithmus durch Wahrheitstabelle -> brute force.
        - **Lösung:**
            - F = (A₀ ⟶ A₂) ⟶ A₁
                ```
                | A₀ A₁ A₂ | A₀ ⟶ A₂ | F   |
                | :------- | :-------- | :-- |
                | 0  0  0  |     1     | *0  |
                | 0  0  1  |     1     | *0  |
                | 0  1  0  |     1     |  1  |
                | 0  1  1  |     1     |  1  |
                | 1  0  0  |    *0     |  1  |
                | 1  0  1  |     1     | *0  |
                | 1  1  0  |    *0     |  1  |
                | 1  1  1  |     1     |  1  |
                ```
                In Zeilen mit * suchen wir Fälle 1 ⟶ 0 und setzen auf 0; sonst auf 1.<br>
                Da manche Zeile 0, mache 1: erfüllbar √; widerlegbar √ (sonst nichts).
            - F = (A₀ ∨ A₁) ∧ ¬(A₀ ∨ A₁)
                <br>
                In jeder Zeile wird (blabla) auf 0 und ¬(blabla) auf 1 oder umgekehrt ausgewertet.<br>
                Darum gilt in jeder Zeile (blabla) ∧ ¬(blabla) = 0.
                <br>
                Da _alle_ Zeile 0: widerlegbar √; nicht erfüllbar √ (sonst nichts).
            - F = A₁ ∨ (A₂ ∧ A₁)
                ```
                | A₁ A₂ | A₂ ∧ A₁ | F   |
                | :---- | :------ | :-- |
                | 0  0  |    0    | *0  |
                | 0  1  |    0    | *0  |
                | 1  0  |    0    |  1  |
                | 1  1  |   ^1    |  1  |
                ```
                In Zeilen mit ^ suchen wir Fälle 1 ∧ 1 und setzen auf 1; sonst auf 0.<br>
                In Zeilen mit * suchen wir Fälle 0 ∨ 0 und setzen auf 0; sonst auf 1.
                <br>
                Da manche Zeile 0, mache 1: erfüllbar √; widerlegbar √ (sonst nichts).
            - F = (A₀ ⟶ A₁) ⟶ (A₁ ⟶ A₀)
                ```
                | A₀ A₁ | A₀ ⟶ A₁ | A₁ ⟶ A₀ | F   |
                | :---- | :------- | :-------- | :-- |
                | 0  0  |    1     |     1     |  1  |
                | 0  1  |    1     |     0     |  0  |
                | 1  0  |    0     |     1     |  1  |
                | 1  1  |    1     |     1     |  1  |
                ```
                In Zeilen mit * suchen wir Fälle 1 ⟶ 0 und setzen auf 0; sonst auf 1.
                <br>
                Da manche Zeile 0, mache 1: erfüllbar √; widerlegbar √ (sonst nichts).

        Insgesamt:
        ```
        | Formel                     | taut. | erf. | wid. | unerf. |
        | :------------------------- | :---- | :--- | :--- | :----- |
        | (A₀ ⟶ A₂) ⟶ A₁          |       |   √  |   √  |        |
        | (A₀ ∨ A₁) ∧ ¬(A₀ ∨ A₁ )    |       |      |   √  |   √    |
        | A₁ ∨ (A₂ ∧ A₁)             |       |   √  |   √  |        |
        | (A₀ ⟶ A₁) ⟶ (A₁ ⟶ A₀) |       |   √  |   √  |        |
        ```
        **‘Parity check’**: Da [taut ⟹ (erf. und nicht wid.)] und [unerf ⟹ (wid. und nicht erf.)],
        sind nur
        ```
        x √ √ x
        √ √ x x
        x x √ √
        ```
        möglich. Wenn man etwas andere als diese 3 Fälle bekommt, hat man einen Fehler gemacht!
- [x] weitere Fragen nach Aufzeichnungsende.

## Nächste Woche ##

- gerne auch bei den Montag + Dienstag Übungen reinschauen, weil wir alle versch. Themen besprechen werden.
- Mittwoch Thema:
    - entweder rekursive Schemata / strukturelle Induktion; oder
    - Bsp. wie man coole Probleme mithilfe auf Erfüllbarkeitsprobleme in der Aussagenlogik kodieren kann.

### TODOs (Studierende) ###

- Folie 3 durchlesen;
- weiter am ÜB 1 arbeiten : )
