# Vorlesungswoche 12 (28. Juni – 4. Juli) #

## Agenda ##

- [x] 2–5 min Organisatorisches
- [x] Serie 5, Seminaraufgaben besprechen
    - [x] SemA 5.1
    - [x] SemA 5.2
    - [x] SemA 5.3
    - [x] SemA 5.4
    - ~~[ ] (Zusatz) SemA 5.5~~ ---> siehe Aufzeichnung
- [x] restliche Zeit für allg. Fragen
    - Alternativer Beweis von (d) durch Argument:

        ```
        »Sonst wären F und F^skol sem. äquivalent,
        aber das ist nicht i. A. der Fall.«
        ```

        Da wir aber uns aber auf einer Instanz beschränken,
        passt dieser Ansatz nicht.
        Wir müssen im Einzelfall prüfen.
        Vgl. Unterschied zw.

        ```
        1. Es ist nicht so, dass (F ≡ F^skol) für alle F
        ```
        und

        ```
        2. Für alle F, es ist nicht so dass (F ≡ F^skol).
        ```

        Wir haben nur 1 und damit kann es durchaus sein,
        dass für einzelne Fälle F, F^skol schon sem. äqv. sind.

## Nächste Woche ##

- HA aus Blatt 4.

### TODOs (Studierende) ###

- am ÜB 5 weiter arbeiten.
