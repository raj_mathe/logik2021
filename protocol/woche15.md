# Vorlesungswoche 15 (19.–25. Juli) #

## Agenda ##

- [x] 2–5 min Organisatorisches
    - [x] Anfragen, ob Vorklausurübung erwünscht wird.
- [x] Serie 5, Hausaufgaben besprechen
    - ~~[ ] allgemeines Feedback~~
    - [x] HA 5.1
    - [x] HA 5.2
    - [x] HA 5.3
    - [x] HA 5.4
    - [x] HA 5.5
- ~~[ ] restliche Zeit für allg. Fragen~~

## Nächste Woche ##

- Studierende bringen **eigene Fragen** mit und wir diskutieren diese.

### TODOs (Studierende) ###

- ÜB 6 bis **Do 22.07.** um 22:00 abgeben!
- Klausurvorbereitung!
