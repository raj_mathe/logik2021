# Vorlesungswoche 4 (3. Mai – 9. Mai) #

Handnotizen findet man unter [notes/woche4.pdf](./../notes/woche4.pdf).

## Agenda ##

- [x] Organisatorisches (max. 5 min)
    - Hochladen = Abgeben
    - Zoom
        - Zugriff
        - Ort der Zoomaufzeichnungen
        - BBB-Aufzeichnung?
- Spezielles Thema: (strukturelle) Rekursion
    - top-down
        - »kleinste Menge«
        - Wohldefiniertheit: einfach!
    - bottom-up
        - wohlfundierte Relationen
        - Wohldefiniertheit: (mühseliger!)
    - Satz: bottom-up = top-down
        - ⊇: weil bottom-up Eigenschaft hat und top-down kleinstmögliche Menge mit Eigenschaft ist.
        - ⊆: (schwerer) zeige, dass alles in Klasse alle _n_-ten Stufen enthält, damit gilt Schnitt aus Klasse (=:top-down) enthält Vereinigung (=:bottom-up)
    - Präsentation (Schemata)
        - für Mengen
            - als Liste
            - durch `"... | ... | ..."` Schreibweise
        - für Funktionen
            - definiere ƒ für Basisfälle.
            - definiere ƒ für Zusammensetzung durch Werte von ƒ auf Teilen.
    - Beispiele:
        - [x] ℕ als „kleinste Menge“, die 0 enthält und unter +1 abgeschlossen ist.
        - [x] eval(·, I)
        - [ ] Atome(·) -> kommt nächste Woche!
        - [ ] Länge -> kommt nächste Woche!
    - Induktion
        - Aus Sicht von bottom-up (-> verallgemeinert Induktion über ℕ)
        - wieso funktioniert es? (Beweis durch Widerspruch -> betrachte `min{x | ¬ ф(x)}`)
            - „min“ hier bzgl. x ≤ y ⟺ x (strikte) Teilformel von y
            - alternativ bzgl. x ≤ y ⟺ |Zeichen in x|  < |Zeichen in y|
        - Beispiele
    - strukturelle Induktion
        - Aus Sicht von top-down (!! neue Sichtweise !!)
        - wieso funktioniert es? (direkter Beweis: `{x | ф(x)} ⊇ »kleinste Menge«`)
        - Beispiele
- [x] weitere Fragen nach Aufzeichnungsende.
    - wie verhält sich das mit Russellmenge?
        -> „gelöst“ durch wohlfundiert/Wohlordnung
        -> R keine Menge

## Nächste Woche ##

- Seminaraufgaben für Serie 2:
    - KNF/DNF
    - Hornformeln / Alg
    - str. Induktion / Rekursion
    - ~~Kompaktheit~~ (-> möglich nach der ÜG)

### TODOs (Studierende) ###

- Folien für Wochen 3--4 durchlesen;
- ÜB 1 bis {22:00 am Donnerstag 6. Mai 2021} abgeben.
