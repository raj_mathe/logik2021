# Vorlesungswoche 5 (10.–16. Mai) #

Handnotizen findet man unter [notes/woche5.pdf](./../notes/woche5.pdf).

## Agenda ##

- [x] 2–5 min Organisatorisches (max. 5 min)
- [x] Serie 2, Seminaraufgaben
    - [x] 7,5 min SA 2.1 Modelle
    - [x] 7,5 min SA 2.2 Wahrheitstafel ~> DNF/KNF
        - DNF nur kurz
        - KNF
    - [x] 5 min SA 2.3 Erkennung von Hornfml
    - [x] 10 min SA 2.4 Markierungsalg
    - [x] 10 min SA 2.5 Strukturelle Ind

## Nächste Woche ##

- Besprechung HA ÜB 1

### TODOs (Studierende) ###

- Folien zu Wochen 3–5 weiterlesen
- weiter am ÜB 2 arbeiten
