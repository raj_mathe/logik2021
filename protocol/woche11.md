# Vorlesungswoche 11 (21.–27. Juni) #

## Agenda ##

- [x] 2–5 min Organisatorisches
- [x] Serie 3, Hausaufgaben besprechen
    - [x] allgemeines Feedback
    - [x] HA 3.1
    - [x] HA 3.2
    - [x] HA 3.3
- [x] restliche Zeit für allg. Fragen
    - Frage über VPN Zugang bis nächste Woche zu klären.

## Nächste Woche ##

- Seminaraufgaben aus Blatt 5.

### TODOs (Studierende) ###

- ÜB 4 bis **Do 24.06.** um 22:00 abgeben!
