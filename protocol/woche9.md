# Vorlesungswoche 9 (7.–13. Juni) #

## Agenda ##

- [x] 2–5 min Organisatorisches
- [x] Serie 2, Hausaufgaben besprechen
    - [x] allgemeines Feedback
    - [x] SemA 2.1
    - [x] SemA 2.2
    - [x] SemA 2.3
    - [x] SemA 2.4
    - [x] SemA 2.5
- [x] restliche Zeit für allg. Fragen

## Nächste Woche ##

- Seminaraufgaben aus Blatt 4.
- Allg.: bitte überlegen, was vor der Klausur erwünscht wäre, bspw. interaktive Sprechstunden/Fragerunde,
    oder ob ihr (selbstverständlich Pandemie-Maßnahmen beachtend) unter euch euch treffen wollt,
    durch den Stoff + die Übungen gemeinsam geht.

### TODOs (Studierende) ###

- ÜB 3 bis **Do 10.06.** um 22:00 abgeben!
