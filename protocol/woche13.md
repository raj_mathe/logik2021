# Vorlesungswoche 13 (5.–11. Juli) #

## Agenda ##

- [x] 2–5 min Organisatorisches
- [x] Serie 4, Hausaufgaben besprechen
    - [x] allgemeines Feedback
    - [x] HA 4.1
    - [x] HA 4.2
    - [x] HA 4.3
    - [x] HA 4.4
    - [x] HA 4.5
- [x] restliche Zeit für allg. Fragen

## Nächste Woche ##

- Seminaraufgaben aus Blatt 6.

### TODOs (Studierende) ###

- ÜB 5 bis **Do 08.07.** um 22:00 abgeben!
