# Logik für Informatiker 2021 #

Diese Repository ist für die Übungsgruppe am Mittwoch.

- Protokolle findet man [hier](./protocol).
- Notizen findet man [hier](./notes).
- Symbolverzeichnis findet man in [notes/glossar.md](./notes/glossar.md).

## LaTeX ##

Folgendes Einführungsvideo hat Emanuel erstellt <https://www.youtube.com/watch?v=HPUGlcZNG2k>.
<br>
Alternativ zu LaTeX kann es evtl. sinnvoller sein, mit Markdown/Pandocs zu arbeiten.

## Wahrheitstabelle-Generator ##

Siehe <https://web.stanford.edu/class/cs103/tools/truth-table-tool>.
Beispiele aus der Übung:

- `(A0 || (A3 -> ! A2))`
- `((A0 -> A2) -> A1)`
- `((A0 || A1) && ! (A0 || A1))`
- `(A1 || (A2 && A1))`
- `((A0 -> A1) -> (A1 -> A0))`

Das Tool ist klug genug, um das Weglassen der äußersten Klammern
und `A0 && A1 && A2` statt `((A0 && A1) && A2)` u. Ä. zu zulassen.

## Zur Klausur ##

- Datum: (siehe VL + Moodle)
- Zulassung: TBA

### Vorbereitung ###

Empfehlung:

1. die Definitionen + Resultate im den VL-Folien komplett durchgehen:
    - prüfen, dass man die Hauptaspekte begreift;
    - strikt zw. den paar Dingen, die wirklich zur Definition / zum Resultat gehören,
        <br/>
        und den Anmerkungen unterscheiden.
2. in die assoziierten Übungsaufgaben schauen und sowohl die Lehre als auch die Anwendungen vertiefen;
3. Stichpunkte von »häufigen Fehlern« (bspw. die man persönlich macht)
    sowie »gewöhnlichen Missverständnissen« erstellen.

Vor der Klausur gut ausschlafen und auf die Ernährung aufpassen.

Während der Klausur:

1. Keine Ablenkungen oder Kommunikationsquellen.
2. Aufgaben sorgfältig durchlesen und auf Antwortformat achten (insbes. bei „leeren“ Feldern).
3. Zeit gut managen. Mal muss man entscheiden _»Soll ich jetzt bei dieser Frage auf Kosten der anderen weitere Zeit verbringen, oder weitermachen und später darauf zurück kommen?«_
4. Übersicht über Fortschritt vor Augen halten.
