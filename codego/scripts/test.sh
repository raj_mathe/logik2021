#!/usr/bin/env bash

################################################################################################
# NOTE: `chmod +x test.sh` vorher ausführen, um dieses Skript benutzen zu können.
################################################################################################

################################
# HILFSMETHODEN
################################

export TEST_VERBOSE=false # <- true: unittest mit verbose output
export TEST_TIMEOUT="10s"

function call_go() {
    go $@;
}

function check_requirements() {
    call_go get "$( cat scripts/requirements )";
}

function run_unittests(){
    echo -e "\033[1mUNITTESTS\033[0m\n";
    local verbose_opt="";
    ( $TEST_VERBOSE ) && verbose_opt="-v"
    call_go test $verbose_opt -timeout $TEST_TIMEOUT -count 1 -run "^Test[A-Z].*" "logik" "./...";
}

################################
# HAUPTVORGÄNGE
################################

# Kann auskommentiert werden, wenn nötige Module schon installiert:
check_requirements;

# Code testen (unittests):
run_unittests;
