#!/usr/bin/env bash

################################################################################################
# NOTE: `chmod +x build.sh` vorher ausführen, um dieses Skript benutzen zu können.
################################################################################################

################################
# HILFSMETHODEN
################################

export NULL="/dev/null"
export ANTLR_VESION="4.7";

function call_go() {
    go $@;
}

function check_requirements() {
    call_go get "$( cat scripts/requirements )";
}

function get_antlr() {
    local url="http://www.antlr.org/download/antlr-${ANTLR_VESION}-complete.jar";
    ( wget $url ) >> $NULL 2> $NULL || (echo -e "[\033[91;1mERROR\033[0m] konnte \033[1;2mwget $url\033[0m nicht ausführen." && exit 1);
    while read fname; do
        if ! [ "$fname" == "" ] && [ -f "$fname" ]; then
            echo -e "\033[92;1mANTLR\033[1m-${ANTLR_VESION}\033[0m wurde heruntergeladen und in \033[1mcodego/grammars\033[0m kopiert.";
            mv "$fname" "grammars/antlr.jar";
            break;
        fi
    done <<< "$( ls antlr*.jar )"
}

function precompile_grammars() {
    local fname;
    local name;
    ! [ -f "grammars/antlr.jar" ] && get_antlr; # <- lädt antl.jar herunter, wenn fehlt
    pushd grammars >> $NULL;
        rm -rf .antlr; # <- wird automatisch erzeugt, aber wir brauche dies nicht
        while read fname; do
            ( [ "$fname" == "" ] || ! [ -f "$fname" ] ) && continue;
            name="$( echo "$fname" | sed -E "s/^(.*)\.g4$/\1/g" )";
            echo -e "\033[92;1mANTLR\033[0m präkompiliert Grammatik \033[1m${fname}\033[0m";
            java -jar antlr.jar -Dlanguage=Go "$fname" -o "$name";
        done <<< "$( ls *.g4 2> $NULL )"
    popd >> $NULL;
}

function compile_programme() {
    [ -f "dist/main" ] && rm "dist/main";
    echo -e "\033[92;1mGO\033[0m kompiliert \033[1mmain.go\033[0m";
    call_go build -o dist/main "main.go";
    ! [ -f "dist/main" ] && exit 1;
}

function run_programme() {
    echo -e "\033[92;1mGO\033[0m kompiliertes Programm wird ausgeführt";
    ./dist/main;
}

################################
# HAUPTVORGÄNGE
################################

# Kann auskommentiert werden, wenn nötige Module schon installiert:
check_requirements;

# Code als Programm kompilieren und ausführen:
precompile_grammars;
compile_programme;
run_programme;
