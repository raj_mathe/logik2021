module logik

go 1.16

require (
	github.com/antlr/antlr4/runtime/Go/antlr v0.0.0-20210510155620-3601dcad5d17
	github.com/joho/godotenv v1.3.0
	github.com/lithammer/dedent v1.1.0
	// siehe https://pkg.go.dev/github.com/stretchr/testify/assert
	// und https://github.com/stretchr/testify
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/tools v0.1.1
)
