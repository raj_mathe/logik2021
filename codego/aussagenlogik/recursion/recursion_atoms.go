package recursion

import (
	"logik/aussagenlogik/formulae"
)

/* ---------------------------------------------------------------- *
 * METHOD: Atoms
 * ---------------------------------------------------------------- */

func Atoms(fml formulae.Formula) []string {
	// Definiere Schema:
	var schema = func(fml formulae.Formula, prevValues [][]string) []string {
		// Herausforderung: schreibe diese Funktion!
		return []string{}
	}
	// Erzeuge Funktion aus Schema und berechne Wert:
	fn := formulae.CreateFromSchemeStringsValued(schema)
	return fn(fml)
}

/* ---------------------------------------------------------------- *
 * METHOD: Subformulae
 * ---------------------------------------------------------------- */

func Subformulae(fml formulae.Formula) []string {
	// Definiere Schema:
	var schema = func(fml formulae.Formula, prevValues [][]string) []string {
		// Herausforderung: schreibe diese Funktion!
		return []string{}
	}
	// Erzeuge Funktion aus Schema und berechne Wert:
	fn := formulae.CreateFromSchemeStringsValued(schema)
	return fn(fml)
}
