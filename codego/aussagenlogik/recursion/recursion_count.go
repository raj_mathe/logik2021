package recursion

import (
	"logik/aussagenlogik/formulae"
)

/* ---------------------------------------------------------------- *
 * METHOD: Formula Depth
 * ---------------------------------------------------------------- */

func FmlDepth(fml formulae.Formula) int {
	// Definiere Schema:
	var schema = func(fml formulae.Formula, prevValues []int) int {
		// Herausforderung: schreibe diese Funktion!
		return 0
	}
	// Erzeuge Funktion aus Schema und berechne Wert:
	fn := formulae.CreateFromSchemeIntValued(schema)
	return fn(fml)
}

/* ---------------------------------------------------------------- *
 * METHOD: Formula Length
 * ---------------------------------------------------------------- */

func FmlLength(fml formulae.Formula) int {
	// Definiere Schema:
	var schema = func(fml formulae.Formula, prevValues []int) int {
		// Herausforderung: schreibe diese Funktion!
		return 0
	}
	// Erzeuge Funktion aus Schema und berechne Wert:
	fn := formulae.CreateFromSchemeIntValued(schema)
	return fn(fml)
}

/* ---------------------------------------------------------------- *
 * METHOD: Number of Parentheses
 * ---------------------------------------------------------------- */
func NrParentheses(fml formulae.Formula) int {
	// Definiere Schema:
	var schema = func(fml formulae.Formula, prevValues []int) int {
		// Herausforderung: schreibe diese Funktion!
		return 0
	}
	// Erzeuge Funktion aus Schema und berechne Wert:
	fn := formulae.CreateFromSchemeIntValued(schema)
	return fn(fml)
}
