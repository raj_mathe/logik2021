package recursion

import (
	"logik/aussagenlogik/formulae"
	"logik/core/utils"
)

/* ---------------------------------------------------------------- *
 * METHOD: Evaluation of fomulae in models
 * ---------------------------------------------------------------- */

func Eval(fml formulae.Formula, I []string) int {
	// Definiere (parameterisiertes) Schema:
	var schema = func(_I []string) func(formulae.Formula, []int) int {
		return func(fml formulae.Formula, prevValues []int) int {
			if fml.IsAtom() || fml.IsGeneric() {
				return utils.BoolToInt(utils.StrListContains(_I, fml.GetName()))
			} else if fml.IsTautologySymbol() {
				return 1
			} else if fml.IsContradictionSymbol() {
				return 0
			} else if fml.IsNegation() {
				return 1 - prevValues[0]
			} else if fml.IsConjunction2() {
				return utils.Min2(prevValues[0], prevValues[1])
			} else if fml.IsConjunction() {
				return utils.MinList(prevValues)
			} else if fml.IsDisjunction2() {
				return utils.Max2(prevValues[0], prevValues[1])
			} else if fml.IsDisjunction() {
				return utils.MaxList(prevValues)
			} else if fml.IsImplication() {
				return utils.BoolToInt(prevValues[0] <= prevValues[1])
			} else if fml.IsDoubleImplication() {
				return utils.BoolToInt(prevValues[0] == prevValues[1])
			} else {
				panic("Could not evaluate expression!")
			}
		}
	}
	// Erzeuge Funktion aus Schema und berechne Wert:
	fn := formulae.CreateFromSchemeIntValued(schema(I))
	return fn(fml)
}
