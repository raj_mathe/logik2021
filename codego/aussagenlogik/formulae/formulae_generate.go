package formulae

// NOTE: GoLang hat noch keine generics. Erst 2022.

/* ---------------------------------------------------------------- *
 * Generate int-value FN from scheme
 * ---------------------------------------------------------------- */

func CreateFromSchemeIntValued(scheme func(fml Formula, prevValues []int) int) func(fml Formula) int {
	var fn func(fml Formula) int
	var subFn = func(ch chan int, subFml Formula) { ch <- fn(subFml) }
	fn = func(fml Formula) int {
		var subFmls = fml.GetSubFormulae()
		var n = len(subFmls)
		var subChan = make([](chan int), n)
		var prevValues = make([]int, len(subFmls))
		// start parallel computations on subformulas
		for i, subFml := range subFmls {
			subChan[i] = make(chan int) // create Channel, since currently nil
			go subFn(subChan[i], subFml)
		}
		// successively read values
		for i := 0; i < n; i++ {
			prevValues[i] = <-subChan[i]
		}
		// apply schema to get value for formula
		return scheme(fml, prevValues)
	}
	return fn
}

/* ---------------------------------------------------------------- *
 * Generate string-value FN from scheme
 * ---------------------------------------------------------------- */

func CreateFromSchemeStringValued(scheme func(fml Formula, prevValues []string) string) func(fml Formula) string {
	var fn func(fml Formula) string
	var subFn = func(ch chan string, subFml Formula) { ch <- fn(subFml) }
	fn = func(fml Formula) string {
		var subFmls = fml.GetSubFormulae()
		var n = len(subFmls)
		var subChan = make([](chan string), n)
		var prevValues = make([]string, len(subFmls))
		// start parallel computations on subformulas
		for i, subFml := range subFmls {
			subChan[i] = make(chan string) // create Channel, since currently nil
			go subFn(subChan[i], subFml)
		}
		// successively read values
		for i := 0; i < n; i++ {
			prevValues[i] = <-subChan[i]
		}
		// apply schema to get value for formula
		return scheme(fml, prevValues)
	}
	return fn
}

/* ---------------------------------------------------------------- *
 * Generate *[]string-value Fn from scheme
 * ---------------------------------------------------------------- */

func CreateFromSchemeStringsValued(scheme func(fml Formula, prevValues [][]string) []string) func(fml Formula) []string {
	var fn func(fml Formula) []string
	var subFn = func(ch chan []string, subFml Formula) { ch <- fn(subFml) }
	fn = func(fml Formula) []string {
		var subFmls = fml.GetSubFormulae()
		var n = len(subFmls)
		var subChan = make([](chan []string), n)
		var prevValues = make([][]string, len(subFmls))
		// start parallel computations on subformulas
		for i, subFml := range subFmls {
			subChan[i] = make(chan []string) // create Channel, since currently nil
			go subFn(subChan[i], subFml)
		}
		// successively read values
		for i := 0; i < n; i++ {
			prevValues[i] = <-subChan[i]
		}
		// apply schema to get value for formula
		return scheme(fml, prevValues)
	}
	return fn
}

/* ---------------------------------------------------------------- *
 * Generate Formula-value Fn from scheme
 * ---------------------------------------------------------------- */

func CreateFromSchemeFmlValued(scheme func(fml Formula, prevValues []Formula) Formula) func(fml Formula) Formula {
	var fn func(fml Formula) Formula
	var subFn = func(ch chan Formula, subFml Formula) { ch <- fn(subFml) }
	fn = func(fml Formula) Formula {
		var subFmls = fml.GetSubFormulae()
		var n = len(subFmls)
		var subChan = make([](chan Formula), n)
		var prevValues = make([]Formula, len(subFmls))
		// start parallel computations on subformulas
		for i, subFml := range subFmls {
			subChan[i] = make(chan Formula) // create Channel, since currently nil
			go subFn(subChan[i], subFml)
		}
		// successively read values
		for i := 0; i < n; i++ {
			prevValues[i] = <-subChan[i]
		}
		// apply schema to get value for formula
		return scheme(fml, prevValues)
	}
	return fn
}

/* ---------------------------------------------------------------- *
 * Generate *[]Formula-value Fn from scheme
 * ---------------------------------------------------------------- */

func CreateFromSchemeFmlsValued(scheme func(fml Formula, prevValues [](*[]Formula)) *[]Formula) func(fml Formula) *[]Formula {
	var fn func(fml Formula) *[]Formula
	var subFn = func(ch chan *[]Formula, subFml Formula) { ch <- fn(subFml) }
	fn = func(fml Formula) *[]Formula {
		var subFmls = fml.GetSubFormulae()
		var n = len(subFmls)
		var subChan = make([](chan *[]Formula), n)
		var prevValues = make([](*[]Formula), len(subFmls))
		// start parallel computations on subformulas
		for i, subFml := range subFmls {
			subChan[i] = make(chan *[]Formula) // create Channel, since currently nil
			go subFn(subChan[i], subFml)
		}
		// successively read values
		for i := 0; i < n; i++ {
			prevValues[i] = <-subChan[i]
		}
		// apply schema to get value for formula
		return scheme(fml, prevValues)
	}
	return fn
}

/* ---------------------------------------------------------------- *
 * Generate {pos: Formula, ne: Formula}-value Fn from scheme
 * ---------------------------------------------------------------- */

func CreateFromSchemeFmlPairValued(scheme func(fml Formula, prevValues []FormulaPair) FormulaPair) func(fml Formula) FormulaPair {
	var fn func(fml Formula) FormulaPair
	var subFn = func(ch chan FormulaPair, subFml Formula) { ch <- fn(subFml) }
	fn = func(fml Formula) FormulaPair {
		var subFmls = fml.GetSubFormulae()
		var n = len(subFmls)
		var subChan = make([](chan FormulaPair), n)
		var prevValues = make([]FormulaPair, len(subFmls))
		// start parallel computations on subformulas
		for i, subFml := range subFmls {
			subChan[i] = make(chan FormulaPair) // create Channel, since currently nil
			go subFn(subChan[i], subFml)
		}
		// successively read values
		for i := 0; i < n; i++ {
			prevValues[i] = <-subChan[i]
		}
		// apply schema to get value for formula
		return scheme(fml, prevValues)
	}
	return fn
}
