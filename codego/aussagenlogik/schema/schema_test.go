package schema_test

/* ---------------------------------------------------------------- *
 * UNIT TESTING
 * ---------------------------------------------------------------- */

import (
	"logik/aussagenlogik/formulae"
	"logik/aussagenlogik/schema"
	"testing"

	"github.com/stretchr/testify/assert"
)

/* ---------------------------------------------------------------- *
 * TESTCASE ParseExpr
 * ---------------------------------------------------------------- */

func TestParseExpr(test *testing.T) {
	var assert = assert.New(test)
	var tree formulae.Formula

	tree = schema.ParseExpr("A8712")
	assert.Equal("A8712", tree.GetExpr())
	assert.Equal("atom", tree.GetKind())
	assert.Equal(0, len(tree.GetSubFormulae()))

	tree = schema.ParseExpr("A12")
	assert.Equal("A12", tree.GetExpr())
	assert.Equal("atom", tree.GetKind())
	assert.Equal(0, len(tree.GetSubFormulae()))

	tree = schema.ParseExpr("  ! A5  ")
	assert.Equal("!A5", tree.GetExpr())
	assert.Equal("not", tree.GetKind())
	assert.Equal(1, len(tree.GetSubFormulae()))

	tree = schema.ParseExpr("A0 -> A1")
	assert.Equal("(A0 -> A1)", tree.GetExpr())
	assert.Equal("implies", tree.GetKind())
	assert.Equal(2, len(tree.GetSubFormulae()))

	tree = schema.ParseExpr("(  A0 && ! !  A1) || A2")
	assert.Equal("((A0 && !!A1) || A2)", tree.GetExpr())
	assert.Equal("or2", tree.GetKind())
	assert.Equal(2, len(tree.GetSubFormulae()))
}
