# Code in golang #

Die Inhalte dieses Ordners sind absolut **kein Pflichtbestandteil** des Kurses.

Diese dienen nur zur Demonstration / konkreten Verwirklichung von Verfahren, die im Kurs auftauchen. Für Wissbegierige mit auch grundlegenden Programmierkenntnissen bietet sich dies als Möglichkeit an, um sich selbst zu überzeugen, dass strukturelle Rekursion funktioniert.

Der Gebrauch dieser Skripte unterliegt der Eigenverantwortung von Studierenden.

Da ich kein Informatiker bin,
sind auch einige Aspekt bestimmt nicht optimal programmiert/strukturiert.
Dafür kann jeder in seiner Kopie einfach alles anpassen.
Das hier soll einfach funktionieren.

## Systemvoraussetzungen ##

- bash (auch bash-for-windows).
- golang (mind. 1.6.x)
- Java8+

Um Schemata in Lexer und Parser zu verwandeln, wird **ANTLR4** gebraucht,
(welches wiederum mithilfe eines Java-Archives kompiliert wird, weshalb man Java benötigt).
Siehe
<https://blog.gopheracademy.com/advent-2017/parsing-with-antlr4-and-go/>
für weitere Informationen dazu.

## Voreinstellungen ##

- In einer bash-console zu diesem Ordner navigieren und folgenden Befehl ausführen:
    ```bash
    chmod +x scripts/*.sh
    ```
- In `scripts/build.sh` gibt es eine Zeile, die zur Kompilierung des Go-Projektes notwendigen Module über **go** installieren lässt.
(Die Liste der Packages findet man in der Datei `scripts/requirements`).
Diese Zeile kann man ruhig nach der ersten Ausführung rauskommentieren.
- Dazu kommt, dass **antlr4.jar** heruntergeladen wird.
Mithilfe dieses Java-Archivs werden aus `grammars/*.g4` go-Skripte für die Grammatik erzeugt.

## Daten ##

In `data.env` kann man Daten (aktuell: auszuwertenden Ausdruck + Interpretation/Modell) eintragen. Man beachte dabei die Syntax.

## Gebrauchshinweise ##

In einer bash-console zu diesem Ordner navigieren und
```bash
source scripts/build.sh
## oder
go build main.go && ./main
```
ausführen.

Das bash Skript macht folgende Schritte

```bash
# installiert go-module (kann nach 1. Mal rauskommentiert werden):
check_requirements;
# lädt ggf. antlr.jar herunter (wenn fehlt), mit dem die Grammatiken erzeugt werden (kann nach 1. Mal rauskommentiert werden):
precompile_grammars;
# kompiliert Go-Projekt (nach jeder Code-Änderung erneut nötig), sonst rauskommentieren:
compile_programme;
# führt kompiliertes Programm auf Daten in data.env aus:
run_programme;
```

## Offene Challenges ##

In dem Ordner `aussagenlogik/recursion` (relativ zum aktuellen Ordner) findet man mehrere leere Methoden (mit dem Kommentar `// Herausforderung...`). Wer es mag, kann versuchen, an seinem Rechner diese Methoden zu definieren und auszuprobieren.

### Händisch testen ###

Probiere es mit Stift-und-Zettel und anhand von Beispielen die Werte händisch zu berechnen. Vergleiche dies mit den durch den Code rekursiv berechneten Werten. Stimmt alles überein?

### Automatisierte Tests ###

Wer etwas standardisierter seine Methoden testen will, kann automatisiertes Testing tätigen.
Diese Tests existieren parallel zu jedem Modul und folgen dem Namensschema `..._test.go`.

- In der Console (wenn noch nicht geschehen) folgenden Befehl einmalig ausführen:
    ```bash
    chmod +x scripts/test.sh
    ```
- In `aussagenlogik/recursion/recursion_test.go` beim relevanten Testteil eine oder mehrere der Zeilen
    ```go
    test.Skip("Methode noch nicht implementiert")
    ```
    rauskommentieren/löschen.
- Jetzt
    ```bash
    source scripts/test.sh
    ```
    ausführen.

Die unittests testen Methoden gegen mehrere vorkonstruierte Testfälle samt erwarteten Ergebnissen geprüft.
Sollten einige Tests scheitern, dann Fehlermeldung durchlesen, und Methode entsprechend der Kritik überarbeiten.

Die geschriebenen Unittests sind natürlich nicht ausführlich. Man kann diese nach Bedarf ergänzen. Am sinnvollsten baut man welche, die wirklich scheitern können, sonst sagen die Tests nichts aus.
