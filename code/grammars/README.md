# LARK #

Die Grammatik wird hier in `.lark` Format präsentiert und gelext+geparsed.
Für Python braucht man `lark`, `lark-parser`, `lark-parser[regex]` über **PIP** zu installieren.
Siehe
<https://lark-parser.readthedocs.io/en/latest/grammar.html>
und
<https://github.com/lark-parser/lark>
für mehr Informationen zu **LARK**.
