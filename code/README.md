# Code #

Die Inhalte dieses Ordners sind absolut **kein Pflichtbestandteil** des Kurses.

Diese dienen nur zur Demonstration / konkreten Verwirklichung von Verfahren, die im Kurs auftauchen. Für Wissbegierige mit auch grundlegenden Programmierkenntnissen bietet sich dies als Möglichkeit an, um sich selbst zu überzeugen, dass strukturelle Rekursion funktioniert.

Der Gebrauch dieser Skripte unterliegt der Eigenverantwortung von Studierenden.

Da ich kein Informatiker bin,
sind auch einige Aspekt bestimmt nicht optimal programmiert/strukturiert.
Dafür kann jeder in seiner Kopie einfach alles anpassen.
Das hier soll einfach funktionieren.

## Systemvoraussetzungen ##

- bash (auch bash-for-windows).
- python (mind. 3.9.x)

## Voreinstellungen ##

- In einer bash-console zu diesem Ordner navigieren und folgenden Befehl ausführen:
    ```bash
    chmod +x scripts/*.sh
    ```
- In `scripts/build.sh` gibt es eine Zeile, die zur Ausführung der Python-Skripte notwendigen Module über PIP installieren lässt. (Die Liste der Packages findet man in der Datei `scripts/requirements`). Diese Zeile kann man ruhig nach der ersten Ausführung rauskommentieren.

## Daten ##

In `data.env` kann man Daten (aktuell: auszuwertenden Ausdruck + Interpretation/Modell) eintragen. Man beachte dabei die Syntax.

## Gebrauchshinweise ##

In einer bash-console zu diesem Ordner navigieren und
```bash
source scripts/build.sh
## oder (für Linux)
python3 main.py
## oder (für Windows)
py -3 main.py
```
ausführen.
Man kann natürlich alles ohne bash machen, wenn man PyCharm o.Ä. besitzt.

## Offene Challenges ##

In der Datei `aussagenlogik/rekursion.py` (relativ zu diesem Ordner) findet man mehrere leere Methoden (mit dem Kommentar `## Herausforderung...`). Wer es mag, kann versuchen, an seinem Rechner diese Methoden zu definieren und auszuprobieren.

### Händisch testen ###

Probiere es mit Stift-und-Zettel und anhand von Beispielen die Werte händisch zu berechnen. Vergleiche dies mit den durch den Code rekursiv berechneten Werten. Stimmt alles überein?

### Automatisierte Tests ###

Wer etwas standardisierter seine Methoden testen will, kann automatisiertes Testing tätigen.
Diese Tests sind im Unterordner `utests` eingetragen und folgen dem Namensschema `..._test.py`.

- In der Console (wenn noch nicht geschehen) folgenden Befehl einmalig ausführen:
    ```bash
    chmod +x scripts/test.sh
    ```
- In `utests/rekursion_test.py` beim relevanten Testteil eine oder mehrere der Zeilen
    ```python
    @unittest.skip('Methode noch nicht implementiert')
    ```
    rauskommentieren/löschen.
- Jetzt
    ```bash
    source scripts/test.sh
    ```
    ausführen.

Die unit tests testen Methoden gegen mehrere vorkonstruierte Testfälle samt erwarteten Ergebnissen geprüft. Sollten einige Tests scheitern, dann Fehlermeldung durchlesen, und Methode entsprechend der Kritik überarbeiten.

Die geschriebenen unit tests sind natürlich nicht ausführlich. Man kann diese nach Bedarf ergänzen. Am sinnvollsten baut man welche, die wirklich scheitern können, sonst sagen die Tests nichts aus.
