#!/usr/bin/env bash

################################################################################################
# NOTE: `chmod +x test.sh` vorher ausführen, um dieses Skript benutzen zu können.
################################################################################################

################################
# HILFSMETHODEN
################################

function call_python() {
    [ "$OSTYPE" == "msys" ] && py -3 $@ || python3 $@;
}

function check_requirements() {
    call_python -m pip install "$( cat scripts/requirements )" >> /dev/null;
}

function run_unittests(){
    echo -e "\033[1mUNITTESTS\033[0m\n";
    local output="$(call_python -m unittest discover -v --top-level-directory "." --start-directory "utests" --pattern "*_test.py" 2>&1)";
    echo -e "$output";
    if ( echo "$output" | grep -E -q "^[[:space:]]*(FAIL:|FAILED)" ); then
        echo -e "[\033[91;1mERROR\033[0m] Unit tests versagt!" && return 1;
    else
        echo -e "[\033[94;1mINFO\033[0m] Unit tests erfolgreich!" && return 0;
    fi
}

################################
# HAUPTVORGÄNGE
################################

# Kann auskommentiert werden, wenn nötige Module schon installiert:
check_requirements;

# Code testen (unittests):
run_unittests;
