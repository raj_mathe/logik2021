#!/usr/bin/env bash

################################################################################################
# NOTE: `chmod +x build.sh` vorher ausführen, um dieses Skript benutzen zu können.
################################################################################################

################################
# HILFSMETHODEN
################################

function call_python() {
    [ "$OSTYPE" == "msys" ] && py -3 $@ || python3 $@;
}

function check_requirements() {
    call_python -m pip install "$( cat scripts/requirements )" >> /dev/null;
}

function run_code() {
    call_python main.py;
}

################################
# HAUPTVORGÄNGE
################################

# Kann auskommentiert werden, wenn nötige Module schon installiert:
check_requirements;

# Code ausführen:
run_code;
